Contact Us
=================

For general questions or discussions about ARA, please email
ara-users@googlegroups.com after joining the `ara-users
<https://groups.google.com/g/ara-users>`_ Google group.

For reporting issues of ARA (e.g., bugs or failures), please email
support@arawireless.org and CC ara-support@iastate.edu.

To reach out to the ARA project team, please email
contact@arawireless.org.



