========================
ARA Baremetal User Guide
========================

The compute component of ARA provides the service of enabling users to launch computing instances to carry out experiments based on their needs. As the ARA provisioning uses a bare-metal setup using real machines to do the computing job, before creating/launching an instance the user needs to create a reservation of nodes first: 

**1.** Use your username and credentials to login

.. image:: ARA_Screenshots/Picture1.png


**2.** After the login was successful, the user was redirected to the dashboard page showing all the resources already used under this project. Go to the “Reservations” tab and click “Leases”

.. image:: ARA_Screenshots/Picture2.png


**3.** Click the “Create lease” button to create the node lease and define how long the compute node will be leased, and how many nodes are needed for the user (By now only two nodes are available for ARA bare-metal provisioning, if a user reserved more than two nodes in one lease the lease creation will fail):

.. image:: ARA_Screenshots/Picture3.png
.. image:: ARA_Screenshots/Picture4.png


**4.** After the lease’s successful creation now the user is able to launch an instance, but before that it is recommended to create a key pair file as this will be needed in the step of instance creation. 

Click the “Key Pairs” tab under the “Compute” category: 

.. image:: ARA_Screenshots/Picture5.png


The users can choose either to create a brand new key pair, or upload an existing one from the local disk. 

Click the “Create Key Pair” button to enter the Key pair name and select the key pair type. The ARA provides SSH Key and X509 Certificate options.

.. image:: ARA_Screenshots/Picture6.png


After the key pair’s successful creation the browser will automatically download the key file in pem format to the computer. 

**5.** Go back to the “Instances” tab under the “Compute” category to start creating a computing instance. Click the “Launch instance” button to enter the instance configuration portal.

.. image:: ARA_Screenshots/Picture7.png


Define the instance’s name and the lease reservation it belongs to: 

.. image:: ARA_Screenshots/Picture8.png


Choose the image which this instance will be using:

.. image:: ARA_Screenshots/Picture9.png


Select the network the instance belongs to: 

.. image:: ARA_Screenshots/Picture10.png


Click the “Launch instance” button to create the instance. The instance may take 10 minutes to spawn. 

.. image:: ARA_Screenshots/Picture11.png


The instances created will need a public IP address so users can access this instance using command line interface. Go to the “Floating IPs” tab under the “Network” category to assign an IP address to the instances. 

.. image:: ARA_Screenshots/Picture12.png


Click the “Allocate IP to project” to create a new IP address. Then click the “Associate” button to allocate the IP address to the newly created instance. 

.. image:: ARA_Screenshots/Picture13.png


It may take several minutes to let the associated IP address be active.

.. image:: ARA_Screenshots/Picture14.png


Once the IP address has been associated with the instance, the users are able to access the instance from Internet using the command line interface.  

**6.** Access the instance

First, change the permission of the key pair file that the instance used:: 

 chmod 600 <keypairfilename>.pem 

Example:: 

 chmod 600 testkeypair.pem

Add the key pair file to the current SSH identity:: 

 ssh-add <keypairfilename>.pem 

If got the “Could not open a connection to your authentication agent” error, execute the following command before the ssh-add:: 

 eval `ssh-agent -s` 

Log in to the instance by SSH using the cc user account and the floating IP address assigned to the instance:: 

 ssh cc@10.24.85.115 (10.24.85.115 is the floating IP address assigned to the instance)




