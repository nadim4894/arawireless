.. _ARA_Resource_Specification:


ARA Resource Specification 
=================================

Resources in ARA are specified using the following three attributes:

1. **Site:** The location/site where the resource resides.
2. **Resource Type**: Represents the domain where the resource is used
   for. In ARA, the resources are classified into three types.
     a. *AraRAN*: Access network resources.
     b. *AraHaul*: Backhaul resources.
     c. *Compute*: Compute resources.
3. **Device Type:** Indicates the type of device under each resource
   type. For example, AraRAN devices include *Base Station* and *nUser
   Equipment*.

The specification of currently deployed resources are provided in the
following table. 

.. list-table:: 
   :widths: 10 15 15 10 15
   :header-rows: 1
   :align: center

   * - Serial No.
     - Resource Name
     - Site
     - Resource Type
     - Device Type
   * - 1
     - ara-compute1-dc
     - Data Center
     - Compute
     - Compute Node
   * - 2
     - WilsonHall-BS
     - Wilson Hall
     - AraRAN
     - Base Station
   * - 3
     - AgronomyFarm-BS
     - Agronomy Farm
     - AraRAN
     - Base Station
   * - 4
     - CurtissFarm-BS
     - Curtiss Farm
     - AraRAN
     - Base Station
   * - 5
     - ResearchPark-BS
     - Research Park
     - AraRAN
     - Base Station
   * - 6
     - Ames-UE-000
     - Ames
     - AraRAN
     - User Equipment
   * - 7
     - CurtissFarm-UE-000
     - Curtiss Farm
     - AraRAN
     - User Equipment
   * - 8
     - KitchenFarm-UE-000
     - Kitchen Farm
     - AraRAN
     - User Equipment
   * - 9
     - AgronomyFarm-UE-000
     - Agronomy Farm
     - AraRAN
     - User Equipment
   * - 10
     - AgronomyFarm-UE-010
     - Agronomy Farm
     - AraRAN
     - User Equipment
   * - 11
     - AgronomyFarm-UE-020
     - Agronomy Farm
     - AraRAN
     - User Equipment

The spatial distribution of the ARA is shown in the following map.

.. raw:: html


         <iframe src="https://www.google.com/maps/d/embed?mid=1MmTVzSlxQxJk24oCBj8Y3n48tQUaprQ&ehbc=2E312F" width="1040" height="480"></iframe>	 


ARA Resource Technical Specification
----------------------------------------

.. _AraRAN_Detailed_Spec:

AraRAN
^^^^^^^^^

As mentioned in :ref:`ARA Infrastructure <ARA_Infrastructure>`, AraRAN
is equipped with different wireless technologies including Software
Defined Radios, Massive MIMO, and COTS 5G solutions. Detailed
specification of different AraRAN components are provided below.

.. _SDR_Detailed_Spec:

Software Defined Radios
*****************************

 * **Base Station**: The SDR BSes are realized using NI USRP N320
   radios with operating frequency 3 MHz to 6 GHz with allowable
   instantaneous bandwidth of 200 MHz. The SDR supports standalone
   (embedded) or host-based (network streaming) operation. The master
   clock rates include 200, 245.76, and 250 MS/s. The SDR is equipped
   with Xilinx Zynq-7100 SoC Dual core ARM Cortex-A9\@800 MHz and
   builtin GPSDO.

   We use CommScope SS-65M-R2 4-port sector antenna  with average gain
   of 18 dBi (4300-3600MHz).

   The RF front-end operates on frequency band 3400-3600 MHz TDD with
   working bandwidth of 100 MHz. The maximum output power is 40 dBm
   and LNA gain of 25 dB. The automatic gain control is up to the
   given maximum output power.

 * **User Equipment**: The SDR UE is equipped with NI USRP B210, first
   fully integrated two channel USRP device with continuous RF
   coverage from 70 MHz - 6 GHz. It supports full duplex MIMO (2 Tx
   and 2 Rx) operation up to 56 MHz of real-time bandwidth (61.44 MS/s
   quadrature). The device supports GNU Radio and OpenBTS support
   through the open source USRP Hardware Driver (UHD). For advanced
   users, the SDR has open and reconfigurable Spartan 6 XC6SLX 150
   FPGA.

   We use 1 x Laird OC69421 direct mount omnidirectional antenna with
   gain of 5 dBi (3300-4200 MHz) and horizontal and vertical plane 3
   dB bandwidth of 360 degrees and 100 degrees, respectively.

   The RF front-end UE booster operates on the frequency band
   3400-3600 MHz TDD with working bandwidth 100 MHz. The maximum
   output power is 30 dBm and LNA gain of 12 dB. The automatic gain
   control is up to the given maximum output power.

.. _Skylark_Detailed_Spec:

Skylark
*********

 * **Base Station**: The base station consists of a Central Unit (CU),
   Distributed Unit (DU), and three Radio Units (RUs), one for each
   sector. Each RU has 7 radios and each radio is connected to a
   cross-polarized antenna having horizontal and vertical
   polarization. That is, each RU has 14x antennas per RU which makes
   a total of 42 antennas for three RUs at the BS covering 360 degrees
   cell area.  The DU can support up to 6 RUs, making total antenna
   count of 84. However, we deployed 3 RUs in Phase-1. The RUs are
   powered via a Power Distribution Unit (PDU) and are connected to
   the DU via FDU.

 * **Customer Premises Equipment (CPE)**: At present, six Skylark
   CPEs are deployed and 14 more CPEs are to be deployed in
   Phase-1. Each CPE has one radio that connects to a dual-polarized
   directional antenna. Omni directional antennas have been
   successfully tested with the CPE, however, the performance at long
   range is better for the directional antenna. Fixed UE nodes are
   equipped with directional antenna mounted on a pole while the
   mobile UEs use omni directional antenna.

 * **Frequency of Operation**: Skylark uses TV White Space (TVWS)
   band, i.e., 470–700 MHz, with target operating band 539- 593
   MHz. Both the BS and CPEs need to tune to same frequency in order
   to establish a connection. The bandwidth supported by the BS is 40
   MHz.


.. _AraHaul_Detailed_Spec:

AraHaul
-----------

The wireless backhaul of ARA primarily consists of microwave and
millimeter wave links and free space optical link. 

.. _Aviat_Detailed_Spec:

Millimeter and Microwave Link
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The long-range mmWave and microwave backhaul is realized using Aviat
WTM 4800 radios. Major features of the platform are as follows:

 * **Carriers**:
    1. E-Band (80 GHz)
      * Bandwidth (MHz): 3.75, 5, 10, 20, 25, 30, 40, 50, 60, 75, 80,
	and 100
      * Modulation: QPSK, 16QAM, 256QAM, 512QAM, 1024QAM, 2048QAM, and
	4096QAM
    2. Microwave (11 GHz)
      * Bandwidth (MHz): 250, 500, 750, 1000, 1500, and 2000
      * Modulation: QUARTER-QPSK, HALF-QPSK, QPSK, 16QAM, 256QAM,
	512QAM, 1024QAM, 2048QAM, and 4096QAM

 * **Capacity**: The dual transceiver design supports
    * Single channel E-band up to 10 Gbps
    * Dual channel multiband combining one E-band and one microwave
      channel aggregates up to 10 Gbps with or without adaptive dual
      carrier capacity.

 * **Power Control**: Fixed automatic transmitter power control (RTPC
   or ATPC)
