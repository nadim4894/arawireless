ARA Storage Service 
==================================

Swift Object Storage
-----------------------------------------------

The Swift Object Service allows users to store and retrieve data in the form of objects with a simple API. It does not have restrictions on the formats of the files uploaded so it's an ideal service for storing unstructured data like images, videos and data sets.

Use cases
-----------------------------------------------

We consider ARA storage service as an easy to access, easy to use permanent storage service for all of the experimenters. With one available user account, users can upload any formats of files and leverage the object store functionality to permanently store them. Once there are any need for these files, users can also easily retrieve them and use them in any required experiments.

Here are several use case examples of where ARA Storage Service can be useful.

     A certain researcher needs to gather monitored data from the experiments. The result of this kind of experiment includes mixed data types like images, videos, audio, data sets in tuples or tables. Traditional relational databases can't handle storing unstrctured data like images or videos. With the ARA Storage Service based on OpenStack Swift. The researchers can upload any files they want to store to the Storage Service. They can also easily download these uploaded files to local machines through either GUI or CLI client if needed.

     One researcher is having a huge amount of sensor data, and he may want to find a place to store them for a long period of time because he may not need to access the data very often. The ARA Storage Service can serve as a repository for archiving raw data for a long period. 

     One experimenter is using the container service also provided ARA to conduct computing. After the computing, this experimenter is able to store anything to his/her own object storage repository. With the OpenStack Swift client, users can access their object storage by using the CLI client with some simple commands. The Swift client enables users to create/delete buckets, upload/download/manipulate objects from a container.

How to Use Swift Service
-----------------------------------------------

After login, click the “Object Store” panel and then click the “Containers” button, to enter the object storage (Swift) portal:

.. image:: images/1.png
     :width: 800
     :align: center

Click the “+Container” button to the container creation portal:

.. image:: images/2.png
     :width: 800
     :align: center

Here you will be able to specify the name, storage policy and container access.

Let’s name the container as “TestBucket1” and click the blue “Submit” button to create it:

.. image:: images/3.png
     :width: 800
     :align: center

After the creation the user should be able to see the properties and available operations of the container just created. The properties include how many objects are stored, the total size of this container, creation date, storage policy and whether this container can be accessed by the public or not:

.. image:: images/4.png
     :width: 800
     :align: center

Click the upload button to enter the objects upload portal:

.. image:: images/5.png
     :width: 800
     :align: center

The Object storage (Swift) does not have any restrictions about the formats of the files uploaded and it supports all kinds of unstructured data types. Here we try to upload an image file in jpg format to demonstrate the capability of Object Storage:

.. image:: images/6.png
     :width: 800
     :align: center

Click the blue “Upload File” to complete the submission.

We will use the same way to upload another text file. All the uploaded file will be displayed like this:

.. image:: images/7.png
     :width: 800
     :align: center

Let’s use the same way to create another empty container named as “TestBucket2”:

.. image:: images/8.png
     :width: 800
     :align: center

Object Storage also allows users to create folders within the containers. The folders serve just like the folders in traditional file systems. Click the “+Folder” button to enter the folder creation portal:

.. image:: images/9.png
     :width: 800
     :align: center

After specifying the Folder Name, click the blue “+Create Folder” button to create the folder.

Any files uploaded to this folder will be stored under this folder directory:

.. image:: images/10.png
     :width: 800
     :align: center

If users want to completely remove one container, just click the trash can icon beside the container tab (Note that before deleting, any objects/folders must be emptied, otherwise the deletion will fail):

.. image:: images/11.png
     :width: 800
     :align: center

After clicking the icon, a confirmation page will pop out. Click the red “Delete” button to finish the container deletion.

.. image:: images/12.png
     :width: 800
     :align: center

Users can also copy one object to another container. Move the mouse cursor to one object and click the down arrow button beside the “Download” button. You should be able to see the options including “View Details”, “Edit”, “Copy” and “Delete”.

.. image:: images/14.png
     :width: 800
     :align: center

Click the “Copy” button to enter the object copy portal, and specify the destination container and object name (Note that there should not be two conflicting objects with the same file name):

.. image:: images/13.png
     :width: 800
     :align: center

Click the blue “Copy Object” to finish

We can see the object has been successfully copied to the destination container we just specified:

.. image:: images/15.png
     :width: 800
     :align: center

Click the “View Details” button to view the details of this object:

.. image:: images/16.png
     :width: 800
     :align: center

How to access the object storage from containers
-----------------------------------------------

With the installation of Swift client on containers. Users will be able to acces their own object storage repository from the container using Swift CLI client.

Use the Swift stat command to display the current status of the overall object storage repository. Use the Swift post command to create a storage bucket.

::

     swift stat
     swift post <bucketname>

.. image:: images/swiftcontainer1.png
     :width: 800
     :align: center


Now you will be able to see that the bucket created in the container can be displayed in the ARAPortal GUI page.

.. image:: images/swiftcontainer2.png
     :width: 800
     :align: center

If the user wants to upload something, use the following command and make sure to specify the desired location and file.

::

     swift upload <bucketname> <filename>

.. image:: images/swiftcontainer3.png
     :width: 800
     :align: center



Now we can see that the selected file (Named as "numbers") has been successfully uploaded and stored.

.. image:: images/swiftcontainer4.png
     :width: 800
     :align: center

If the user wants to download something from the object storage to their instances. The swift download command will do the job:

::

     swift download <bucketname> <filename>


In this tutorial we are trying to download the "swiftring" file from the repository:

.. image:: images/swiftcontainer5.png
     :width: 800
     :align: center


We can see that the file "swiftring" has been successfully downloaded to the container and the experimenter can already access it.


.. image:: images/swiftcontainer6.png
     :width: 800
     :align: center
