Software Images Available in ARA
=============================================

For ARA users, we provide pre-built container images for
experimentation. The container images are equipped with software tools
specifically for the domain of experimentation. For instance, the
containers for RAN experiments are equipped with USRP Hardware Driver
(UHD) and OpenSource 5G stacks for Base Stations and User
Equipment. For AraHaul and other types of AraRAN experiments, we
create containers with wrapper APIs for measurement and
configuration. The following table provides the description of
container images made available for ARA users. 


.. list-table:: 
   :widths: 5 15 35
   :header-rows: 1
   :align: center

   * - Serial No.
     - Image ID
     - Description
   * - 1
     - ``aravisor/container_images:gNodeB``
     - The image provides the software for OpenAirInterface gNodeB
       functionality.
   * - 2
     - ``aravisor/container_images:nrUE``
     - Software for OpenAirInterface NR-UE
   * - 3
     - ``arasklk/ara-sklk:sklk-api_v0.3``
     - The container provides wrapper APIs for Skylark experiments. 


