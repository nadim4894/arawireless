Getting Started with ARA Portal
==========================================================

ARA Portal is the primary interface for users to login to ARA
environment and explore the resources. Different components of the
web-portal are described below.

ARA Login
-----------------

The first step for any user for entering the ARA platform is to
authenticate using the appropriate credentials, i.e., username and
password. Since the primary interface for ARA platform is the
web-portal, users need to visit the `ARA Portal`_ where they can see
the login page with options to provide username and password. A
snapshot of the ARA login screen is shown below. 

.. _ARA Portal: https://portal.arawireless.org/

.. figure:: images/Login_Screen.png
   :align: center
   :width: 400


ARA Dashboard 
------------------------------------

On login, the user will be taken to an overview page consisting of a
dashboard and a description of the usage of resources including
compute, memory, storage, and network as shown below.

.. figure:: images/Resource_Overview.png
   :align: center
   :width: 1000

The dashboard primarily consists of three menus: (1) Project, (2)
Admin (for project administrators), and (3) Identity.

Project
^^^^^^^^^^

The *Project* menu offers users options to explore ARA resources such
as compute nodes, storage nodes, base stations, and user
equipment. For managing the resources, *Project* menu provides three
sub-menus for *Compute*, *Container*, *Network*, and
*Reservations*.

1. **Compute**: The *Compute* sub-menu provide information on the usage
   of ARA resources including compute instances, CPU usage, memory
   usage, network resource (such as floating point IPs) usage.

2. **Containers**: The sub-menu is provided for containerized resource
   management. That is, users can create containers on reserved
   resources, access the container console, execute experiments, stop
   containers, and delete the containers. The page corresponding to
   the *Containers* tab lists the existing containers from where users
   can view containers' information and manage them.

3. **Network**: The *Network* sub-menu is intended at managing the
   network resources. The *Network Topology* tab provides a graphical
   view of existing networks in the project. The *Networks* tab allows
   users to create a new network for the project so that the compute
   resources can communicate each other through the network. The
   *Routers* tab allows users to create virtual routers that can
   connect different networks in the project. The *Floating IPs* tab
   enables users to associate external IP, i.e., other than the
   default IP from the connected virtual network, to their compute
   resources (containers) so that the compute resources can be
   accessed from the jump-box node.

4. **Reservations**: Before using any resource, users should reserve the
   resource or a create a lease on the resource. The *Lease* tab under
   reservations allow users to view the resources along with their
   availability (through the host calendar) and create lease on the
   required resource.

Identity
^^^^^^^^^^^^

The *Identity* menu is provided specifically to manage user identity
credentials and view the projects the user is involved in. 


Resource Calendar
---------------------------------------

As far as ARA users are concerned, it is important to select the
appropriate resources for the experiments. Since ARA is a multiuser
platform, it is possible that the required resources may be already
reserved for other experiments. In this context, host calendar feature
of AraSoft provides a visual representation of the resource
availability so that users can reserve the resources depending on
their availability.

.. figure:: images/Resource_Calendar.png
   :align: center




Reserving ARA Resources
---------------------------------------------

On finding appropriate resource from the resource calendar, users need
to reserve or create a lease for the resource. The *Lease* tab under
the *Reservations* sub-menu helps user to create a lease for the
resource. While creating the lease users should provide a name as well
as the duration for the lease. Further, the specification of the
resource the user needs to be provided which includes the site where
the resource resides, type of the resource (RAN, backHaul, or
compute), and the device type such as base station, user equipment, or
compute node. An example for reserving a resource is provided in the
:ref:`Hello World <ARA_Hello_World>` experiment.


How to Access the Reserved Resource
-------------------------------------------------------

ARA follows containerized resource provisioning approach and the
reserved resources can be accessed via containers. In other words, ARA
enables the users to launch containers on the reserved nodes, thereby
providing access to the resources including the wireless radios. On
launching a container, users are provided access to the resources via
the console from the web-interface or through SSH via the jump-box. An
example for launching a container on the reserved resource is provided
in the :ref:`Hello World <ARA_Hello_World>` experiment.

