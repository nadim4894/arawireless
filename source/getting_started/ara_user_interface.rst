ARA User Interface
========================================

In ARA, we envision to provide users with different interfaces, such
as web-portal, Command Line Interface (CLI), and Jupyter Notebook
interface, for using ARA infrastructure and perform experiments. For
Phase-1, we provide the web-portal as the primary mode of access. The
CLI and Notebook based interfaces are under development. 



ARA Portal 
------------------

The web-portal of ARA, we call as ARA Portal or AraPortal, forms the
graphical user interface for ARA. The portal enables a user to login
to ARA environment, explore the ARA resources, reserve resources, and
perform experiments. The portal includes a dashboard with different
menus for managing compute resource reservations, container
management, network management, and user identity management. Each
menu takes users specific web-pages for managing specific
resources. For instance, using the Containers menu in dashboard, users
can create, launch, execute commands, and stop containers.




..
   ARA Command Line Interface (CLI) 
   ---------------------------------------------------------------------


..
   ARA Jupyter Interface
   --------------------------------------------------------

