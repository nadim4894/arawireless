How to Login to ARA?
===============================================

The first step for any user for entering the ARA platform is to
authenticate using the appropriate credentials, i.e., username and
password. Since the primary interface for ARA platform is the
web-portal, users need to visit the `ARA Portal`_ where they can see
the login page with options to provide username and password. A
snapshot of the ARA login screen is shown below. 

.. _ARA Portal: https://portal.arawireless.org/

.. figure:: images/Login_Screen.png
   :align: center
   :width: 400

On login, the user will be taken to an overview page where the status
of usage of resources including compute, memory, storage, and network
as shown below.

.. figure:: images/Resource_Overview.png
   :align: center
   :width: 1000




   
