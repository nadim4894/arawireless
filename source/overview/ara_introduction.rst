What is ARA?
==============================================

ARA, acronym for **A**\gricultural and ru\ **RA**\l communities, is an
at-scale platform for advanced wireless research deployed across the
Iowa State University (ISU) campus, City of Ames (where ISU resides),
and surrounding research and producer farms as well as rural
communities in central Iowa, spanning a rural area with diameter over
60km. ARA is envisioned to serve as a wireless living lab for smart
and connected rural communities, enabling the research and development
of rural-focused wireless technologies that provide affordable,
high-capacity connectivity to rural communities and industries such as
agriculture.

ARA infrastructure includes a wide variety of wireless technologies
including low-UHF mMIMO, mmWave, sub-7GHz, microwave, satellite, and
free space optical communication offering bandwidth ranging from a few
megabits per second to hundreds of gigabits per second. The wireless
infrastructure deployment spans across the rural environment for
exploiting the scope of applications such as precision agriculture
(involving robots, autonomous vehicles, camera, and sensors),
phenotyping, and smart livestock farms. Besides the rural-specific
application, ARA includes user equipment deployed in public platforms
of community living including transportation, water resource, and fire
and safety services. 



