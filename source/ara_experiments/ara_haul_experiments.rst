AraHaul Experiments
===================================================

Microwave and Millimeter Wave Experiments
-------------------------------------------------

Aviat (WTM 4800) radios are used for wireless backhaul connections
between the base station and core network using microwave (11 GHz) and
mmWave (80 GHz) frequency bands. Each WTM 4800 radio can be equipped
with one or two transceivers configured for a variety of operational
modes, including Single Transceiver Single-Band, Dual-Transceiver
Single-Band, and Dual Transceiver Multi-Band. From the very beginning,
the experimenter needs to know how to establish the wireless link
first, illustrated as below:

**Experiment specification:**

In order to provide the wireless backhaul connection for rurual
regions, and develop the solutions for more wireless technics like
link adaptation, QoS flow control, experimenters need to establish the
link first.

**ARA resources used:**

* AraHaul nodes: two times of Aviat Networks mmWave and microwave
  nodes and host computer

**AraSoft capabilities leveraged:**

* User interface, experimentation workflow support
* Instrumentation service

  * Automation of experiment software deployment at different nodes

* Measurement service

  * Experimentation context information for post-experiment data
    analysis

**Deliverables of experiment:**

* Measurement data for different carriers

  * Signal statistics of different carriers: signal strength, SNR,
    BER, data throughput, etc.

