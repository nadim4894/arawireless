.. Sample Document documentation master file, created by
   sphinx-quickstart on Wed Jun 23 23:50:14 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

ARA User Manual
===============

..
   The ARA software platform, called AraSoft, is designed to provide
   users the experience of ARA infrastructure in terms of resource
   identification, specification, reservation, and provisioning. The
   AraSoft makes use of OpenStack components to provide the required
   services to the users. AraSoft allows users to provision resources in
   two ways: (1) Container Mode called **AraCont** and (2) Bare-metal
   Mode, called **AraBare**.

.. toctree::
   :maxdepth: 2
   :caption: Overview

   overview/ara_introduction
   overview/ara_infrastructure
   ..
      overview/ara_basic_concepts

.. toctree::
   :maxdepth: 2
   :caption: Getting Started

   ..
      getting_started/joining_a_project
   getting_started/ara_use_policy
   getting_started/ara_user_interface
   ..
      getting_started/user_login

   getting_started/started_with_ara_portal
   ..
      getting_started/started_with_ara_cli
      getting_started/started_with_ara_jupyter
   

.. toctree::
   :maxdepth: 2
   :caption: ARA Technical Manual

   ara_technical_manual/ara_resource_specification
   ara_technical_manual/images
   ..
      ara_technical_manual/sandbox_service
      ara_technical_manual/ara_measurement_service
      ara_technical_manual/ara_storage_service
   ..
      ara_technical_manual/ara_byod_service
      ara_technical_manual/ara_cocreation_service
	     
.. toctree::
   :maxdepth: 2
   :caption: ARA Experiments

   ara_experiments/ara_hello_world
   ara_experiments/ara_ran_experiments
   ..
      ara_experiments/ara_haul_experiments
      ara_experiments/ara_e2e_experiments

.. toctree::
   :maxdepth: 2
   :caption: Help

   ..
      help/ara_help
   help/ara_contact
