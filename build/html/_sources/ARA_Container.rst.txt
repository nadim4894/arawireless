Getting Started with AraCont
============================

Similar to OpenStack, ARA users need to have authentication
credentials to access the ARA infrastructure. Upon login using a valid
username and password (provided by the ARA administrator), users are
redirected to the dashboard where they can navigate to different
resources and options provided in ARA. In AraCont configuration, users
can reserve the resources in container mode by creating lease for a
particular time duration. The following provides step-by-step
procedure for reserving resources in ARA.

1. Access the login page and use your credentials to login.

.. image:: ARA_Screenshots/Login.png
     :width: 400
     :align: center

|
 
..
   2. After successful login, you should be able to see the dashboard,
      including the containers that already created.

   .. image:: ARA_Screenshots/2.png
	:align: center

   |
 
2. To reserve an ARA resource, the user needs to create a lease as
   follows:

   * Click the **Leases** option under the “**Reservations**” tab in
     the dashboard. On the right side you can click on the “**Create
     Lease**” button.


.. image:: ARA_Screenshots/Lease_Menu.png
     :align: center

|

3. The user will be redirected to the **Create Lease** form for
   specifying the reservation specific attributes. 

   a. In the tab **General**, you need to specify the following
      attributes:

        * *Lease Name*: User given name for the lease.
        *  *Start Date*: The intended date of start of the lease.
        *  *Start Time*: The intended time of the start of the lease on the *Start Date*.
        * *Lease Length*: The number of days intended for the lease.
        * *Ends*: The intended date when reservation ends.
        * *End Time*: The intended time at which reservation ends on the date specified in the field *Ends*. 

       .. image:: ARA_Screenshots/General_Tab.png
            :width: 600
            :align: center

    b. In the tab **Networks**, users can select the network they want
       to use for the communication between the reserved resources.

         * *Network Name*: The user given name for the network.
	 * *Network Description*: A description of the network
	   provided by the user.
	 * *Resource Properties*: From this drop-down box, users can
	   select the network they want to reserve.

       .. image:: ARA_Screenshots/Networks_Tab.png
	    :width: 600
            :align: center       

    c. In the tab **Devices**, you can select the number of devices
       that you want to reserve with specific set of attributes such
       as site type (e.g., *Base Station* or *User Equipment*) and site
       location (such as *Research Park* or *Wilson Hall*). The
       attributes can be added to the device by clicking the **Add
       filter** button.  The fields in the tab include:

         * *Minimum Number of Devices*: The minimum number of devices
	   you want to reserve.
	 * *Maximum Number of Devices*: The maximum number of devices
	   you want to reserve.
	 * *Resource Properties*: The attributes of resources such as
	   device type, site location, and vendor.

       .. image:: ARA_Screenshots/Devices_Tab.png
	    :width: 600
            :align: center

      d. In the **Wireless Parameters** tab, you can select the center
	 frequency and the bandwidth for the selected devices in the
	 **Devices** tab. 

	 * *Center Frequency*: The center frequency on which the
	   devices (BS and UEs) operates.
	 * *Bandwidth*: The bandwidth needs to be reserved for use by
	   the devices.

	 In the following figure, the *Center Frequency* is assigned
	 to 20GHz and bandwidth to 10GHz. That is, the frequency range
	 15GHz-25GHz is reserved for the user. 

       .. image:: ARA_Screenshots/Wireless_Parameters_Tab.png
	    :width: 600
            :align: center



       After specifying the lease parameters, click the “**Create**”
       button at the bottom right to create the lease. It may take
       several minutes to activate the lease.

4. To view the lease information, you can click the *Lease* tab under
   the **Reservations** menu in the dashboard. The created leases will
   be listed in the page and click on any lease to see the
   information. A sample screenshot of a lease is shown below.

   .. image:: ARA_Screenshots/Lease_Details.png
        :width: 500
        :align: center

  Remember to copy the lease ID of the containers because the ID will be
  required during the container creation.

5. To create a container, select the **Containers** tab under the
   “**Container**” menu in the dashboard as shown in the following
   figure.

   .. image:: ARA_Screenshots/Containers_Tab.png
	:width: 1000
	:align: center

   | 

   a. Click the “**+ Create Container**” button at the top
      right of the page as in the following figure.

      * In the **Networks** tab, select the network.
	(Here we use the “*sharednet1*” as example)

	.. image:: ARA_Screenshots/Networks_Menu.png
	     :width: 800
	     :align: center

      * From the **Security Groups** menu, select the security group
	from the list of selected security groups. After selecting the
	group, press the button with arrow pointing upward.

	.. image:: ARA_Screenshots/Security_Groups_Tab.png
	     :width: 800
	     :align: center

       * In the tab **Scheduler Hints**, add the *Reservation ID* in
	 the **reservation** field and click the **+** button and
	 click "**Create**" button.

	 .. image:: ARA_Screenshots/Scheduler_Hints_Tab.png
	      :width: 800
	      :align: center

         The same process can be repeated for creating different
	 containers, however, providing the appropriate *Lease ID*. 

	 ..
	    .. image:: ARA_Screenshots/14.png
		 :width: 800
		 :align: center

6. After the containers are in “**running**” state, we can assign
   floating IP addresses to them. Select “**Network**” menu from the
   dashboard then “Floating IP” tab:

   .. image:: ARA_Screenshots/Floating_IPs_Menu.png
        :width: 800
	:align: center

  * Click the “**Associate**” button and select a port to be
    associated. For example, to assign a floating point IP address to
    the container launched at the Base Station, container’s IP
    address, i.e., 10.0.0.207, needs to be selected as the port in the
    following form. Further, click the “Associate” button. 

    .. image:: ARA_Screenshots/Associate_Window.png
	 :width: 600
	 :align: center

    Upon associating the floating point IP, the container can be
    accessed using the floating point IP via SSH. 
